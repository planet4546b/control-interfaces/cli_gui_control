#ifndef __DIVING_SYSTEM_H
#define __DIVING_SYSTEM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    DS_DIVING_STATE,
    DS_SURFACING_STATE,
    DS_UNKNOWN_STATE,
} ds_state_t;

void ds_go_bottom(void);
void ds_up(void);
void ds_stop_anything(void);
ds_state_t ds_get_state(void);

#ifdef __cplusplus
}
#endif

#endif /* __DIVING_SYSTEM_H */
