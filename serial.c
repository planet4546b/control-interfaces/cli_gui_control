#include "serial.h"
#include "cli_config.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>

#include <fcntl.h>
#include <unistd.h>

#include "logger.h"

static int fd = 0;

void serial_init(void)
{
    fd = open(SUB_SERIAL_FILE_NAME, O_WRONLY | O_SYNC);

    if (fd < 0) {
        logger_debug_out("[Serial][ERR] can't open uart file. Ret code = %d\n", fd);
        exit(EXIT_FAILURE);
    }

    logger_debug_out("[Serial] init success\n");
}

void serial_deinit(void)
{
    close(fd);
}

void serial_send(uint8_t *data, size_t len)
{
    write(fd, data, len);
    usleep(1000 * SUB_SERIAL_SENDING_DELAY_MS);
}
