#ifndef __SERIAL_H
#define __SERIAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

void serial_init(void);
void serial_deinit(void);
void serial_send(uint8_t *data, size_t len);

#ifdef __cplusplus
}
#endif

#endif /* __SERIAL_H */
