#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>

#include "cli_config.h"
#include "logger.h"

#include <ncurses.h>

static FILE *logfile_ptr = NULL;

void logger_init(void)
{
    time_t rawtime;
    struct tm * timeinfo;

    logfile_ptr = fopen(SUB_CLI_FILE_NAME, "a");
    if (!logfile_ptr) {
        exit(EXIT_FAILURE);
    }

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    logger_debug_out("\n\nLOGGING STARTED: %s", asctime(timeinfo));
}

void logger_deinit(void)
{
    fclose(logfile_ptr);
}

void logger_debug_out(const char *format, ...)
{
    static char log_buf[1024] = { 0 };
    size_t len = 0;

    va_list args;
    va_start(args, format);
    len = vsnprintf((char *)log_buf, sizeof(log_buf), format, args);
    va_end(args);

    fwrite(log_buf, len, 1, logfile_ptr);
}
