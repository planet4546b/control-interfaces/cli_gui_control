TARGET = sub_cli

# CROSS_COMPILER = arm-none-eabi-
# The gcc compiler bin path can be either defined in make command via GCC_PATH variable (> make GCC_PATH=xxx)
# either it can be added to the PATH environment variable.
ifdef GCC_PATH
CC = $(GCC_PATH)/$(CROSS_COMPILER)gcc
else
CC = $(CROSS_COMPILER)gcc
endif

C_SOURCES = main.c \
			thrusters.c \
			logger.c \
			led.c \
			diving_system.c \
			serial.c \
	
C_INCLUDES = -I./

C_DEFS =

LIBS = -lncurses

CFLAGS = $(INCLUDES) $(C_DEFS) -Wall 

all: 
	$(CC) $(CFLAGS) $(LIBS) $(C_SOURCES) -o $(TARGET)

clean:
	rm -rf *.o $(TARGET)