#ifndef __LOGGER_H
#define __LOGGER_H

#ifdef __cplusplus
extern "C" {
#endif

void logger_init(void);
void logger_deinit(void);
void logger_debug_out(const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif /* __LOGGER_H */
