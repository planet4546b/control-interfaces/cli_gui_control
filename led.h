#ifndef __LED_H
#define __LED_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void led_on(void);
void led_off(void);
void led_toggle(void);
int led_get_state(void);

#ifdef __cplusplus
}
#endif

#endif /* __LED_H */
