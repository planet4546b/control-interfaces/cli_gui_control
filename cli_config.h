#ifndef __CLI_CONFIG_H
#define __CLI_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

/* Logger */
#define SUB_CLI_USE_LOGGER 1
#define SUB_CLI_FILE_NAME "sub_cli.log"

/* Border */
#define SUB_CLI_BORDER_TOP_SYMB '~'
#define SUB_CLI_BORDER_BOTTOM_SYMB '~'
#define SUB_CLI_BORDER_LEFT_SYMB '-'
#define SUB_CLI_BORDER_RIGHT_SYMB '-'

#define SUB_CLI_BORDER_REDUCE_COEFF   4
#define SUB_CLI_BORDER_SHIFT_FROM_BORDER 2

/* thrusters */
#define SUB_THRUSTERS_SPEED_STEP (2U)

/* serial */
#define SUB_SERIAL_FILE_NAME "/dev/ttyS2"
#define SUB_SERIAL_SENDING_DELAY_MS 100

#ifdef __cplusplus
}
#endif

#endif /* __CLI_CONFIG_H */
