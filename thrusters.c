#include "thrusters.h"
#include "cli_config.h"

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include "serial.h"

static int left_speed = 0;
static int right_speed = 0;

void thrusters_both_speed_step_up(void)
{
    uint8_t data1[] = { 0x01, 0x04, 0x00, 0x0A };
    uint8_t data2[] = { 0x01, 0x06, 0x00, 0x0A };

    serial_send(data1, sizeof(data1));
    serial_send(data2, sizeof(data2));

    if (left_speed < 100) {
        left_speed += SUB_THRUSTERS_SPEED_STEP;
    }

    if (right_speed < 100) {
        right_speed += SUB_THRUSTERS_SPEED_STEP;
    }
}

void thrusters_both_speed_step_down(void)
{
    uint8_t data1[] = { 0x01, 0x05, 0x00, 0x0A };
    uint8_t data2[] = { 0x01, 0x07, 0x00, 0x0A };

    serial_send(data1, sizeof(data1));
    serial_send(data2, sizeof(data2));

    if (left_speed > -100) {
        left_speed -= SUB_THRUSTERS_SPEED_STEP;
    }

    if (right_speed > -100) {
        right_speed -= SUB_THRUSTERS_SPEED_STEP;
    }
}

void thrusters_turn_left_step(void)
{
    uint8_t data1[] = { 0x01, 0x05, 0x00, 0x0A };
    uint8_t data2[] = { 0x01, 0x06, 0x00, 0x0A };

    serial_send(data1, sizeof(data1));
    serial_send(data2, sizeof(data2));

    if (left_speed > -100) {
        left_speed -= SUB_THRUSTERS_SPEED_STEP;
    }

    if (right_speed < 100) {
        right_speed += SUB_THRUSTERS_SPEED_STEP;
    }
}

void thrusters_turn_right_step(void)
{
    uint8_t data1[] = { 0x01, 0x04, 0x00, 0x0A };
    uint8_t data2[] = { 0x01, 0x07, 0x00, 0x0A };

    serial_send(data1, sizeof(data1));
    serial_send(data2, sizeof(data2));

    if (left_speed < 100) {
        left_speed += SUB_THRUSTERS_SPEED_STEP;
    }

    if (right_speed > -100) {
        right_speed -= SUB_THRUSTERS_SPEED_STEP;
    }
}

void thrusters_stop_all(void)
{
    uint8_t data[] = { 0x01, 0x03, 0x0A };

    serial_send(data, sizeof(data));
    serial_send(data, sizeof(data));
    serial_send(data, sizeof(data));

    left_speed = 0;
    right_speed = 0;
}

int thrusters_get_left_speed(void)
{
    return left_speed;
}

int thrusters_get_right_speed(void)
{
    return right_speed;
}
