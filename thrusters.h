#ifndef __THRUSTERS_H
#define __THRUSTERS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void thrusters_both_speed_step_up(void);
void thrusters_both_speed_step_down(void);
void thrusters_turn_left_step(void);
void thrusters_turn_right_step(void);
void thrusters_stop_all(void);

int thrusters_get_left_speed(void);
int thrusters_get_right_speed(void);

#ifdef __cplusplus
}
#endif

#endif /* __THRUSTERS_H */
