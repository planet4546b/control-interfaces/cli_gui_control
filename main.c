#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ncurses.h>

#include "thrusters.h"
#include "logger.h"
#include "cli_config.h"
#include "led.h"
#include "diving_system.h"
#include "serial.h"

static void draw_border(void);
static void draw_thrusters_speed(void);
static void draw_led_state(void);
static void draw_ds_state(void);

static void terminal_colors_init(void);

int main (int argc, char *argv[])
{
    int c = -1;

    initscr();
    logger_init();
    logger_debug_out("[Sub CLI] application started\n"); /* report that CLI has been started */

    serial_init(); /* open uart file */

    noecho(); /* don't print user entered symbols */
    curs_set(0); /* blinking off */
    keypad(stdscr, 1); /* allow arrows */
    terminal_colors_init(); /* init colors */

    /* draw window to display data */
    draw_border();

    do {
        if (c == KEY_UP || c == 'w' || c == 'W') {
            thrusters_both_speed_step_up();
        } else if (c == KEY_DOWN || c == 's' || c == 'S') {
            thrusters_both_speed_step_down();
        } else if (c == KEY_LEFT || c == 'a' || c == 'A') {
            thrusters_turn_left_step();
        } else if (c ==KEY_RIGHT || c == 'd' || c == 'D') {
            thrusters_turn_right_step();
        } else if (c == 'p' || c == 'P') {
            thrusters_stop_all();
            ds_stop_anything();
        } else if (c == 'l' || c == 'L') {
            led_toggle();
        } else if (c == 'u' || c == 'U') {
            ds_up();
        } else if (c == 'b' || c == 'B') {
            ds_go_bottom();
        }

        draw_thrusters_speed();
        draw_led_state();
        draw_ds_state();
    } while ((c = getch()) != 27); /* ESC code */

    thrusters_stop_all();
    serial_deinit();
    logger_deinit();
    endwin();

    return EXIT_SUCCESS;
}

static void draw_border(void)
{
    int x, y, x_shift, y_shift;

    getmaxyx(stdscr, y, x);
    logger_debug_out("[%s] max[y]=%d max[x]=%d\n", __func__, y, x);

    x_shift = x / SUB_CLI_BORDER_REDUCE_COEFF;
    y_shift = y / SUB_CLI_BORDER_REDUCE_COEFF;

    logger_debug_out("[%s] y_shift=%d x_shift=%d\n", __func__, y_shift, x_shift);

    /* top horizontal */
    for (int i = x_shift; i < x - x_shift; ++i) {
        mvprintw(y_shift, i, "%c", (int)SUB_CLI_BORDER_TOP_SYMB);
    }

    /* bottom horizontal */
    for (int i = x_shift; i < x - x_shift; ++i) {
        mvprintw(y - y_shift, i, "%c", (int)SUB_CLI_BORDER_BOTTOM_SYMB);
    }

    /* left vertical */
    for (int i = y_shift; i < y - y_shift; ++i) {
        mvprintw(i, x_shift, "%c", (int)SUB_CLI_BORDER_LEFT_SYMB);
    }

    /* right vertical */
    for (int i = y_shift; i < y - y_shift; ++i) {
        mvprintw(i, x - x_shift, "%c", (int)SUB_CLI_BORDER_RIGHT_SYMB);
    }
}

static void draw_thrusters_speed(void)
{
    int x, y, x_shift, y_shift, left_speed, right_speed, border_xsize;

    getmaxyx(stdscr, y, x);

    x_shift = x / SUB_CLI_BORDER_REDUCE_COEFF;
    y_shift = y / SUB_CLI_BORDER_REDUCE_COEFF;

    border_xsize = x - 2 * x_shift;

    left_speed = thrusters_get_left_speed();
    right_speed = thrusters_get_right_speed();

    logger_debug_out("[%s][LEFT] speed = %d\n", __func__, left_speed);
    logger_debug_out("[%s][right] speed = %d\n", __func__, right_speed);

    mvprintw(y_shift + SUB_CLI_BORDER_SHIFT_FROM_BORDER, /* y */
             x_shift + border_xsize / 2 - 18, /* x */
             "Thruster [L] speed  =  %d%%\n",
             left_speed);
    mvprintw(y_shift + 2 * SUB_CLI_BORDER_SHIFT_FROM_BORDER,  /* y */
             x_shift + border_xsize / 2 - 18, /* x */
             "Thruster [R] speed  =  %d%%\n",
             right_speed);
}

static void draw_led_state(void)
{
    int x, y, x_shift, y_shift, led_state, border_xsize;

    getmaxyx(stdscr, y, x);

    led_state = led_get_state();
    x_shift = x / SUB_CLI_BORDER_REDUCE_COEFF;
    y_shift = y / SUB_CLI_BORDER_REDUCE_COEFF;
    border_xsize = x - 2 * x_shift;

    logger_debug_out("[%s] LEDs state = %d\n", __func__, led_state);

    mvprintw(y_shift + 3 * SUB_CLI_BORDER_SHIFT_FROM_BORDER, /* y */
             x_shift + border_xsize / 2 - 15, /* x */
             "LED state = %d (%s)\n",
             led_state,
             (led_state == 1) ? "On" : "Off");
}

static void draw_ds_state(void)
{
    int x, y, x_shift, y_shift, ds_state, border_xsize;
    char *state_str;

    getmaxyx(stdscr, y, x);

    ds_state = ds_get_state();
    x_shift = x / SUB_CLI_BORDER_REDUCE_COEFF;
    y_shift = y / SUB_CLI_BORDER_REDUCE_COEFF;
    border_xsize = x - 2 * x_shift;

    if (ds_state == DS_DIVING_STATE) {
        state_str = "Diving";
    } else if (ds_state == DS_SURFACING_STATE) {
        state_str = "Surfacing";
    } else {
        state_str = "Unknown";
    }

    logger_debug_out("[%s] DS state = %d (%s)\n", __func__, ds_state, state_str);

    mvprintw(y_shift + 4 * SUB_CLI_BORDER_SHIFT_FROM_BORDER, /* y */
             x_shift + border_xsize / 2 - 15, /* x */
             "DS state = %d (%s)\n",
             ds_state,
             state_str);
}

static void terminal_colors_init(void)
{
    /* Initializes eight basic colors (black, red, green, yellow, blue, magenta, cyan, and white) */
    start_color();

    if (has_colors()) {
        logger_debug_out("[Sub CLI] terminal DOES support colors\n");
    } else {
        logger_debug_out("[Sub CLI] terminal DOESN'T support colors\n");
    }

    if (can_change_color()) {
        logger_debug_out("[Sub CLI] terminal CAN change colors\n");
    } else {
        logger_debug_out("[Sub CLI] terminal CAN'T colors\n");
    }
}
