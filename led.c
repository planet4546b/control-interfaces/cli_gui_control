#include "led.h"
#include "cli_config.h"

#include <stdlib.h>
#include <stdint.h>

#include "serial.h"

static int led_state = 0;

void led_on(void)
{
    led_state = 1;

    uint8_t data[] = { 0x00, 0x01, 0x0A };
    serial_send(data, sizeof(data));
}

void led_off(void)
{
    led_state = 0;

    uint8_t data[] = { 0x00, 0x02, 0x0A };
    serial_send(data, sizeof(data));
}

void led_toggle(void)
{
    if (led_state) {
        led_off();
    } else {
        led_on();
    }
}

int led_get_state(void)
{
    return led_state;
}
