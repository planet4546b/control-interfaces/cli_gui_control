#include "diving_system.h"
#include "cli_config.h"

#include <stdlib.h>
#include <stdint.h>

#include "serial.h"

static ds_state_t ds_state = DS_UNKNOWN_STATE;

void ds_go_bottom(void)
{
    uint8_t data1[] = { 0x02, 0x03, 0x0A };
    uint8_t data2[] = { 0x02, 0x04, 0x0A };

    serial_send(data1, sizeof(data1));
    serial_send(data2, sizeof(data2));

    ds_state = DS_DIVING_STATE;
}

void ds_up(void)
{
    uint8_t data1[] = { 0x02, 0x03, 0x0A };
    uint8_t data2[] = { 0x02, 0x05, 0x0A };

    serial_send(data1, sizeof(data1));
    serial_send(data2, sizeof(data2));

    ds_state = DS_SURFACING_STATE;
}

void ds_stop_anything(void)
{
    uint8_t data[] = { 0x02, 0x03, 0x0A };

    ds_state = DS_UNKNOWN_STATE;

    serial_send(data, sizeof(data));
    serial_send(data, sizeof(data));
    serial_send(data, sizeof(data));
}

ds_state_t ds_get_state(void)
{
    return ds_state;
}
